package com.ibeetl.eval;

import org.antlr.v4.runtime.tree.TerminalNode;
import org.beetl.core.AntlrProgramBuilder;
import org.beetl.core.GroupTemplate;
import org.beetl.core.parser.BeetlParser.ExpressionContext;
import org.beetl.core.parser.BeetlParser.MuldivmodExpContext;
import org.beetl.core.statement.ArthExpression;
import org.beetl.core.statement.Expression;

import com.ibeetl.eval.ast.ExpressionWrapper;
import com.ibeetl.eval.ast.SafeArthExpression;

public class EvalAntlrProgramBuilder extends AntlrProgramBuilder {

	public EvalAntlrProgramBuilder(GroupTemplate gt) {
		super(gt);
	}
	
	@Override
	protected ArthExpression parseMuldivmodExpression(MuldivmodExpContext ctx)
	{
		Expression a = this.parseExpress(ctx.expression(0));
		Expression b = this.parseExpress(ctx.expression(1));
		a = new ExpressionWrapper(a,getContent(ctx.expression(0)));
		b= new ExpressionWrapper(b,getContent(ctx.expression(1)));
		TerminalNode tn = (TerminalNode) ctx.children.get(1);
		short mode = 0;
		if (ctx.MUlTIP() != null)
		{
			mode = ArthExpression.MUL;
		}
		else if (ctx.DIV() != null)
		{
			mode = ArthExpression.DIV;
		}
		else if (ctx.MOD() != null)
		{
			mode = ArthExpression.MOD;
		}
		ArthExpression temp = new  ArthExpression(a, b, mode, this.getBTToken(tn.getSymbol()));
		SafeArthExpression safeArth = new SafeArthExpression(temp);
		return safeArth;
	}
	
	/*取得表达式字符串*/
	protected String getContent(ExpressionContext ctx){
		return ctx.getText();
	}

}
