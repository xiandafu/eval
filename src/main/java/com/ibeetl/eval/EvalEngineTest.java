package com.ibeetl.eval;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class EvalEngineTest {
	
	
	public static void main(String[] args) throws Exception {
//		testEval();
//		testScript();
//		testDefine();
//		testCondition();
//		testLookup();
//		testErrorCheck();
		testExceptionData();
		

	}
	
	public static void testExceptionData() throws Exception{
		EvalEngine engine = new EvalEngine();
		Map<String,Object> paras = new HashMap<String,Object>();
		//分别测试这俩项目数据为0情况。
		paras.put("年收益",2);
		paras.put("年成本", 1);
		InputStream ins = EvalEngineTest.class.getResourceAsStream("/sample/exception.xml");
		InputStreamReader reader = new InputStreamReader(ins,"utf8");
		TreeNode ret = engine.run(reader, paras);
		PrintTreeNode ptn = new PrintTreeNode(ret);
		System.out.println(ret.toString());
		System.out.println(ptn.toString());
	}
	
	public static void testErrorCheck() throws Exception{
		EvalEngine engine = new EvalEngine();
		
		InputStream ins = EvalEngineTest.class.getResourceAsStream("/sample/error.xml");
		InputStreamReader reader = new InputStreamReader(ins,"utf8");
		String error = engine.validate(reader);
		System.out.println(error);
	}
	
	public static void testLookup() throws Exception{
		EvalEngine engine = new EvalEngine();
		Map<String,Object> paras = new HashMap<String,Object>();
		paras.put("答案1", "yes");
		paras.put("答案2", "no");
		InputStream ins = EvalEngineTest.class.getResourceAsStream("/sample/simple005.xml");
		InputStreamReader reader = new InputStreamReader(ins,"utf8");
		TreeNode ret = engine.run(reader, paras);
		PrintTreeNode ptn = new PrintTreeNode(ret);
		System.out.println(ret.toString());
		System.out.println(ptn.toString());
	}
	
	public static void testCondition() throws Exception{
		EvalEngine engine = new EvalEngine();
		Map<String,Object> paras = new HashMap<String,Object>();
		paras.put("房产", 0);
		paras.put("已婚", false);
		paras.put("年收入", 6000000);
		
		InputStream ins = EvalEngineTest.class.getResourceAsStream("/sample/simple004.xml");
		InputStreamReader reader = new InputStreamReader(ins,"utf8");
		TreeNode ret = engine.run(reader, paras);
		PrintTreeNode ptn = new PrintTreeNode(ret);
		System.out.println(ret.toString());
		System.out.println(ptn.toString());
		
	}
	
	public static void testDefine() throws Exception{
		EvalEngine engine = new EvalEngine();
		Map<String,Object> paras = new HashMap<String,Object>();
		InputStream ins = EvalEngineTest.class.getResourceAsStream("/sample/simple003.xml");
		InputStreamReader reader = new InputStreamReader(ins,"utf8");
		TreeNode ret = engine.run(reader, paras);
		System.out.println("ouput:"+ret);
	}
	public static void testScript() throws Exception{
		
		EvalEngine engine = new EvalEngine();
		Map<String,Object> paras = new HashMap<String,Object>();
		paras.put("上访次数", 2);
		InputStream ins = EvalEngineTest.class.getResourceAsStream("/sample/simple002.xml");
		InputStreamReader reader = new InputStreamReader(ins,"utf8");
		TreeNode ret = engine.run(reader, paras);
		System.out.println(ret);
	}
	public static void testEval() throws Exception{
			
		EvalEngine engine = new EvalEngine();
		Map<String,Object> paras = new HashMap<String,Object>();
		paras.put("每月收入", 178832323.12);
		paras.put("每月负债", 128.99);
		InputStream ins = EvalEngineTest.class.getResourceAsStream("/sample/simple001.xml");
		InputStreamReader reader = new InputStreamReader(ins,"utf8");
		TreeNode ret = engine.run(reader, paras);
		System.out.println(ret);
	}
	
	

}
