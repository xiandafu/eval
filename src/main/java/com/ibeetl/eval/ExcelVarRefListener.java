package com.ibeetl.eval;

import java.util.Stack;

import org.beetl.core.Event;
import org.beetl.core.Listener;
import org.beetl.core.statement.PlaceholderST;
import org.beetl.core.statement.VarRef;

import com.ibeetl.eval.ast.ExcelVarRef;

public class ExcelVarRefListener implements Listener {

	
	public ExcelVarRefListener(){
		
	}
	@SuppressWarnings("rawtypes")
	public Object onEvent(Event e) {
		Stack stack = (Stack) e.getEventTaget();
		Object o = stack.peek();
		if (o instanceof VarRef) {
			VarRef ref = (VarRef) o;
			ExcelVarRef sqlRef = new ExcelVarRef(ref);
			return sqlRef;
		} else {
			return null;
		}
	}
}
