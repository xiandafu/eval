package com.ibeetl.eval;

import org.jdom2.Element;

public interface  NodeWorker {
	
	public  TreeNode execute(Element ele,EvalContext ctx);
	public default String getScript(Element ele){
		String express =  Util.getXMLContent(ele);
		return "return "+express+";";
	}
}
