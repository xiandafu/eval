package com.ibeetl.eval;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

public class PrintTreeNode {
	TreeNode root = null;
	public PrintTreeNode(TreeNode root){
		this.root = root;
	}
	
	public String toString(){
		//四个空格
		return this.toString("    ");
	}
	
	private String toString(String tab){
		int tabCount = 0;
		StringWriter sw = new StringWriter();
		print(root,sw,tab,tabCount);
		return sw.toString();
	}
	
	private void print(TreeNode node,Writer w,String tab,int tabCount){
		tabCount = tabCount+1;
		try {
			outputTab(w,tab,tabCount);
			w.write("["+node.getNodeName()+"]["+node.getResult()+"]\n");
			for(TreeNode child:node.getChildren()){
				
				print(child, w, tab,tabCount);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void outputTab(Writer w,String tab,int tabCount){
		for(int i=0;i<tabCount;i++){
			try {
				w.write(tab);
			} catch (IOException e) {
				return ;
			}
		}
	}
}
