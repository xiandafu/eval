package com.ibeetl.eval;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.exception.ScriptEvalError;
import org.beetl.core.resource.StringTemplateResourceLoader;

public class Test {
	static GroupTemplate gt = null;
	static {
		 try {
			gt = new GroupTemplate(new StringTemplateResourceLoader(),Configuration.defaultConfiguration());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) throws Exception {
		testEval();

	}
	public static void testEval() throws IOException{
			String template = "return 测试得分*权重;";
			Map paras = new HashMap();
			paras.put("测试得分", 12);
			paras.put("权重", 3);
			Map ret = null;
			try {
				 ret = gt.runScript(template, paras);
			} catch (ScriptEvalError e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Object result = ret.get("return");
			System.out.println(result);
			
	}
	
	

}
