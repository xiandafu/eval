package com.ibeetl.eval;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 计算结果，包含了整个树的运行结果.如果不关心整颗数的计算过程，直接取出根节点的结果
 * @author xiandafu
 *
 */
public class TreeNode {
	Object result;
	@JsonIgnore
	TreeNode parentNode;
	@JsonProperty("name") 
	String nodeName;
	@JsonIgnore
	Object nodeRefInfo;
	//在计算节点值“前”的参数值
	@JsonIgnore
	Map<String,Object> paras = new HashMap<String,Object>();
	@JsonProperty("nodes") 
	List<TreeNode> children = new ArrayList<TreeNode>();
	public Object getResult() {
		return result;
	}
	public void setResult(Object result) {
		this.result = result;
	}
	public TreeNode getParentNode() {
		return parentNode;
	}
	public void setParentNode(TreeNode parentNode) {
		this.parentNode = parentNode;
	}
	public String getNodeName() {
		return nodeName;
	}
	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}
	public Object getNodeRefInfo() {
		return nodeRefInfo;
	}
	public void setNodeRefInfo(Object nodeRefInfo) {
		this.nodeRefInfo = nodeRefInfo;
	}
	public List<TreeNode> getChildren() {
		return children;
	}
	public void setChildren(List<TreeNode> children) {
		this.children = children;
	}
	
	
	
	public Map<String, Object> getParas() {
		return paras;
	}
	public void setParas(Map<String, Object> paras) {
		this.paras = paras;
	}
	@Override
	public String toString() {
		return "TreeNode [result=" + result + ", nodeName=" + nodeName + "]";
	}
	
	/**
	 * 查找离自己最近的变量
	 * @param name
	 * @return
	 */
	public TreeNode findTreeNodeByName(String name){
		for(TreeNode node :children){
			TreeNode find = findTreeNode(node,name);
			if(find!=null){
				return find;
			}
		}
		return null;
	}
	
	private  TreeNode findTreeNode(TreeNode parent,String name){
		for(TreeNode node :parent.getChildren()){
			if(node.getNodeName().equals(name)){
				return node;
			}else{
				TreeNode findNode =  findTreeNode(node,name);
				if(findNode!=null){
					return findNode;
				}else{
					continue;
				}
			}
		}
		return null;
	}
	
	
}
