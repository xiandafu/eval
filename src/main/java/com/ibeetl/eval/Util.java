package com.ibeetl.eval;

import java.util.List;

import org.jdom2.CDATA;
import org.jdom2.Content;
import org.jdom2.Element;
import org.jdom2.Text;

public class Util {
	public static String getXMLContent(Element el){
		if(el.getChildren().size()!=0){
			return null;
		}
		StringBuilder sb = new StringBuilder();
		List<Content> list = el.getContent();
		for(Content c:list){
			if(c instanceof Text){
				Text text = (Text)c;
				sb.append(text.getText());
			}else  if (c instanceof CDATA) {
				 CDATA cdata = (CDATA) c;  
				 sb.append(cdata.getText());
			}
		}
		return sb.toString();
	}
}
