package com.ibeetl.eval;

import org.beetl.core.exception.ScriptEvalError;
import org.jdom2.Element;

public class XmlEvalException extends RuntimeException{
	Element el;
	
	public XmlEvalException(Element el,String error){
		super(error);
		this.el = el;
	}
	
	public XmlEvalException(Element el,ScriptEvalError e){
		super(null,e);
		this.el = el;
	}
}
