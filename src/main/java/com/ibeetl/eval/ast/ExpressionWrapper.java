package com.ibeetl.eval.ast;

import org.beetl.core.Context;
import org.beetl.core.InferContext;
import org.beetl.core.statement.Expression;
import org.beetl.core.statement.GrammarToken;

public class ExpressionWrapper extends Expression {

	//原始表达式
	Expression expression;
	//表达式字符串
	String expStr;
	public ExpressionWrapper(Expression expression,String expStr) {
		super(expression.token);
		this.expression = expression;
		this.expStr = expStr;
	}
	
	public Object evaluate(Context ctx)
	{
		return expression.evaluate(ctx);
	}

	@Override
	public void infer(InferContext inferCtx) {
		expression.infer(inferCtx);;

	}

}
