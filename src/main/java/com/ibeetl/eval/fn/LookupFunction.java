package com.ibeetl.eval.fn;

import java.util.Map;

import org.beetl.core.Context;
import org.beetl.core.Function;

public class LookupFunction implements Function {

	public Object call(Object[] paras, Context ctx) {
		Object key = paras[0];
		Map map = (Map)paras[1];
		if(map.containsKey(key)){
			Object target = map.get(key);
			return target;
		}else{
			if(paras.length==3){
				return paras[2];
			}else{
				return null;
			}
		}
		
		
	}
	
}
