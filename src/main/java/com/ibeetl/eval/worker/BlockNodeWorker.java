package com.ibeetl.eval.worker;

import java.util.List;

import org.jdom2.Element;

import com.ibeetl.eval.EvalContext;
import com.ibeetl.eval.NodeWorker;
import com.ibeetl.eval.TreeNode;

public class BlockNodeWorker implements NodeWorker {

	public  TreeNode execute(Element ruleEle,EvalContext ctx){
		String name = ruleEle.getAttributeValue("name");
		TreeNode node =  ctx.enter(name, ruleEle);
		List<Element> list = ruleEle.getChildren();
		List<TreeNode> children = ctx.getEvalEngine().processChildren(list, ctx);
		ctx.exist();
		//定义，没有任何结果。是否用其他变量来代替null含义
		node.setResult(null);
		return node;
	}
	
}
