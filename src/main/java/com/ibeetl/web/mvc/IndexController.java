package com.ibeetl.web.mvc;

import java.io.StringReader;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibeetl.eval.EvalEngine;
import com.ibeetl.eval.PrintTreeNode;
import com.ibeetl.eval.TreeNode;

@Controller

public class IndexController {
	@RequestMapping("/")
	public ModelAndView index(){
		ModelAndView view = new ModelAndView("/001.html");
		return view;
	}
	
	@RequestMapping("/{model}.html")
	public ModelAndView index(@PathVariable() String model){
		ModelAndView view = new ModelAndView("/"+model+".html");
		return view;
	}
	
	@RequestMapping("/run.json")
	public @ResponseBody String index(String code,HttpServletRequest req){
		EvalEngine engine = new EvalEngine();
		Map<String,Object> paras = getMap(req);
		
		TreeNode ret;
		JsonResult result  = null;
		try {
			ret = engine.run(new StringReader(code), paras);
			PrintTreeNode ptn = new PrintTreeNode(ret);
			String message = ptn.toString();
			result = new JsonResult(ret.getResult(),message);
		} catch (Exception e) {
			e.printStackTrace();
			result = new JsonResult("脚本计算错误,详情查看后台日志"); 
		}
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			String tree = mapper.writeValueAsString(result);
			return tree;
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	@RequestMapping("/validate.json")
	public @ResponseBody String validate(String code,HttpServletRequest req){
		EvalEngine engine = new EvalEngine();
		JsonResult result = null;
		try {
			String ret = engine.validate(new StringReader(code));
			
			result = new JsonResult(ret,ret);
		} catch (Exception e) {
			e.printStackTrace();
			result = new JsonResult("脚本计算错误,详情查看后台日志"); 
		}
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			String tree = mapper.writeValueAsString(result);
			return tree;
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	

	
	
	private Map getMap (HttpServletRequest req){
		Map<String,Object> map = new HashMap<String,Object>();
		for(int i=0;i<10;i++){
			String name = req.getParameter("paraName"+i);
			String value = req.getParameter("paraValue"+i);
			if(name!=null&&name.length()!=0){
				Object realValue = this.getValue(value);
				map.put(name, realValue);
			}
			
		}
		return map;
	}
	
	//简单的类型获取
	private Object getValue(String value){
		try{
			BigDecimal b = new BigDecimal(value);
			return b;
		}catch(Exception ex){
			return value;
		}
	}
	
	
	
}
