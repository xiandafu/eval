package com.ibeetl.web.mvc;
public  class JsonResult{
	boolean success = true;
	String message = "";
	Object data = null;
	public JsonResult(Object result,String message){
		data = result;
		this.message = message;
	}
	public JsonResult(String message){
		this.success = false ;
		this.message = message;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	
	
}
